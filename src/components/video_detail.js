import React from 'react'

const VideoDetail = ({ video }) => {
  if (!video) {
    return (
      <div className='col-xs-12 col-sm-8'>
        <div className='d-flex justify-content-center loading-wrap'>
          <i className='material-icons loading'>autorenew</i> Loading
        </div>
      </div>
    )
  }

  const id = video.id.videoId
  const url = `https://youtube.com/embed/${id}`
  return (
    <div className='col-xs-12 col-sm-8'>
      <div className='card'>
        <div className='embed-responsive embed-responsive-16by9 card-img-top'>
          <iframe title={video.snippet.title} className='embed-responsive-item' src={url} />
        </div>

        <div className='card-body'>
          <p className='card-text lead'>
            <strong>{video.snippet.title}</strong><br />
            <small>{video.snippet.channelTitle}</small>
          </p>
          <p className='card-tex'>{video.snippet.description}</p>
        </div>
      </div>
    </div>
  )
}

export default VideoDetail
