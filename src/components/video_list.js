import React from 'react'
import VideoItem from './video_list_item'

const VideoList = ({ videos, onVideoSelect }) => {
  const videoItem = videos.map((video) => <VideoItem key={video.etag} selectedVideo={video => onVideoSelect(video)} video={video} />)

  return (
    <div className='col-xs-12 col-sm-4'>
      <ul className='list-group'>
        {videoItem}
      </ul>
    </div>
  )
}

export default VideoList
