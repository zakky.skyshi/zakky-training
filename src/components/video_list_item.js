import React from 'react'

const VideoItem = ({ video, selectedVideo }) => {
  const imgUrl = video.snippet.thumbnails.default.url

  return (
    <li onClick={() => selectedVideo(video)} className='list-group-item video-list-item' title={video.snippet.title}>
      <div className='media'>
        <img className='mr-3' src={imgUrl} alt={video.snippet.channelTitle} />
        <div className='media-body'>
          <p className='mt-0'>
            <strong>{video.snippet.title}</strong> <br />
            <small>{video.snippet.channelTitle}</small>
          </p>
        </div>
      </div>
    </li>
  )
}

export default VideoItem
