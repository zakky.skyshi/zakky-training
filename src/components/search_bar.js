import React, { Component } from 'react'

class SearchBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      term: '',
      label: 'control-label'
    }
  }

  render () {
    const Label = ({theClass}) => <label className={theClass}>Search</label>

    return (
      <div className='col'>
        <div className='card search'>
          <div className='card-body'>
            <input id='searchbar' onChange={e => this.onValueChange(e.target.value)} className='form-control' />
            <Label theClass={this.state.label} />
          </div>
        </div>
      </div>
    )
  }

  onValueChange (term) {
    this.setState({ term })
    if (term === '') {
      this.setState({ label: 'control-label' })
    } else {
      this.setState({ label: 'control-label active' })
    }
    this.props.onSearchChange(term)
  }
}

export default SearchBar
