import React, { Component } from 'react'
import YTSearch from 'youtube-api-search'
import _ from 'lodash'
import SearchBar from './components/search_bar'
import VideoList from './components/video_list'
import VideoDetail from './components/video_detail'

const API_KEY = 'AIzaSyAL8pDjAmKnj6Y29KS4H_y6TLVw9CN_YAU'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      videos: [],
      selectedVideo: ''
    }

    this.youtubeSearch('NCS')
  }

  youtubeSearch (term) {
    YTSearch({ key: API_KEY, term: term }, (videos) => {
      this.setState({ videos })
      this.setState({ selectedVideo: videos[0] })
    })
  }

  searchTerm (term) {
    this.youtubeSearch(term)
  }

  render () {
    const searchTerm = _.debounce(term => { this.searchTerm(term) }, 500)

    return (
      <div className='container'>
        <div className='row'>
          <SearchBar onSearchChange={(term) => searchTerm(term)} />
        </div>
        <div className='row'>
          <VideoDetail video={this.state.selectedVideo} />
          <VideoList videos={this.state.videos} onVideoSelect={video => this.setState({ selectedVideo: video })} />
        </div>
      </div>
    )
  }
}

export default App
